What is a database?
A database is an organized collection of information or data.

Data vs Information
Data is raw and does not carry any specific meaning. 
Information on the other hand is a group of organized data that contains logical meaning.

Databases typically refer to information stored in a computer system but it can also refer to physical databases.
One such example is a library which is a database of books.

A database is managed by what is called a database management system.

What is a database management system?
A database management system (or DBMS) is a system specifically designed to manage the storage, retrieval and modification of data in a database.

Continuing on the example earlier, the DBMS of a physical library is the Dewey Decimal System.

CRUD operations
Database systems allows four types of operations when it comes to handling data, namely create (insert), read (select), update and delete. 

Relational Database
Relational database is a type of database where data is stored as a set of tables with rows and columns (pretty much like a table printed on a piece of paper).

NoSQL Database
For unstructured data (data that cannot fit into a strict tabular format), NoSQL databases are commonly used.



What is SQL?
SQL stands for structured query language. It is the language used typically in relational DBMS to store, retrieve and modify data.

SQL databases require tables and information provided in it’s columns to be defined before they are created and then used to store information. This ensures that information stored in SQL databases to be of the same data structure preventing any errors of storing data that are incomplete.

Because of SQL databases require tables and columns to be defined before creation and use, NoSQL databases are becoming more popular due to their flexibility of being able to change the data structure of information in databases on the fly.
Developers can omit the process of recreating tables and backing up data and re importing them as needed on certain occasions.

What is NoSQL?
NoSQL means Not only SQL. NoSQL was conceptualized when capturing complex, unstructured data became more difficult. 
One of the popular NoSQL databases is MongoDB.

What is MongoDB?
MongoDB is an open-source database and the leading NoSQL database. 
Its language is highly expressive, and generally friendly to those already familiar with the JSON structure.
Mongo in MongoDB is a part of the word humongous which then means huge or enormous.

The main advantage of this data structure is that it is stored in MongoDB using the JSON format. 
A developer does not need to keep in mind the difference between writing code for the program itself and data access in a DBMS.

Also, in MongoDB, some terms in relational databases will be changed:
from tables to collections;
from rows to documents for rows; and
from columns to fields

What is a data model?
A data model describes how data is organized or group in a database.
By creating data models, we anticipate which data will be managed by the database system and the application to be developed.

Relationships can have three types:
One-to-one
E.g. A person can only have one employee ID on a given company.
One-to-many
E.g. A person can have one or more email addresses.
Many-to-many
E.g. A book can be written by multiple authors and an author can write multiple books.

Entity-relationship diagram
An entity-relationship diagram (or ERD) is a diagram used to describe the structure of the database design.
It aims to show what attributes (fields) an entity (collection) has. It also shows the relationship of attributes between entities.
